
use std::ops::RangeInclusive;


fn parse_line(line: &str) -> (RangeInclusive<u32>, RangeInclusive<u32>) {
    let ranges = line.split(",");
    let mut range_pair = Vec::new();

    for range in ranges {
        let end_points = range.split("-")
                              .map(|n| n.parse::<u32>().unwrap())
                              .collect::<Vec<_>>();

        range_pair.push(end_points);
    }
    (RangeInclusive::new(range_pair[0][0], range_pair[0][1]),
     RangeInclusive::new(range_pair[1][0], range_pair[1][1]))
}


fn count_overlaps(list_of_pairs: &str, f: fn(RangeInclusive<u32>, RangeInclusive<u32>) -> bool) -> u32 {
    list_of_pairs.lines().map(|line| {
        let (range1, range2) = parse_line(line);
        f(range1, range2) as u32
    }).sum::<u32>()
}


fn fully_overlapping(range1: RangeInclusive<u32>, range2: RangeInclusive<u32>) -> bool {
    range1.clone().all(|x| range2.contains(&x)) ||
    range2.clone().all(|x| range1.contains(&x))
}



fn partial_overlapping(mut range1: RangeInclusive<u32>, range2: RangeInclusive<u32>) -> bool {
    range1.any(|x| range2.contains(&x))
}


fn main() {
    println!("Part 1: {}", count_overlaps(include_str!("input.txt"), fully_overlapping));
    println!("Part 2: {}", count_overlaps(include_str!("input.txt"), partial_overlapping));
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_line() {
        let (r1, r2) = parse_line("95-98,47-96");

        assert_eq!(r1, RangeInclusive::new(95, 98));
        assert_eq!(r2, RangeInclusive::new(47, 96));
    }


    #[test]
    fn test_fully_overlapping() {
        assert_eq!(count_overlaps(include_str!("test.txt"), fully_overlapping), 2);
    }


    #[test]
    fn test_partial_overlapping() {
        assert_eq!(count_overlaps(include_str!("test.txt"), partial_overlapping), 4);
    }
}
