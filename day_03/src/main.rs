
use std::collections::HashSet;


fn item_to_prio(item: &char) -> u32 {
    let prio = " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    if let Some(idx) = prio.find(*item) {
        return idx as u32;
    } else {
        panic!("Didn't find the item ({}) in the prio list", item);
    }
}


fn compute_sum_prio(rucksacks: &str) -> u32 {
    rucksacks.lines().map(|line| {
        let parts: Vec<HashSet<char>> = vec![
            HashSet::from_iter(line[..line.len()/2].chars()),
            HashSet::from_iter(line[line.len()/2..].chars()),
        ];
        if let Some(item) = parts[0].intersection(&parts[1]).next() {
            item_to_prio(item)
        } else {
            panic!("No itersetion element");
        }
    }).sum::<u32>()
}


fn compute_sum_group_prio(rucksacks: &str) -> u32 {
    let mut sum_prio = 0;
    let mut lines = rucksacks.lines();

    loop {
        if let Some(line) = lines.next() {
            let mut intersection: HashSet<char> = HashSet::from_iter(line.chars());
            for _ in 1..3 {
                if let Some(line) = lines.next() {
                    let elf_items: HashSet<char> = HashSet::from_iter(line.chars());
                    intersection.retain(|item| elf_items.contains(item));
                } else {
                    panic!("Could not complete the group of three!");
                }
            }
            assert_eq!(intersection.len(), 1);
            sum_prio += item_to_prio(intersection.iter().next().unwrap());
        } else {
            break;
        }
    }

    sum_prio
}


fn main() {
    println!("Part 1: {}", compute_sum_prio(include_str!("input.txt")));
    println!("Part 2: {}", compute_sum_group_prio(include_str!("input.txt")));
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_compute_sum_prio() {
        assert_eq!(compute_sum_prio(include_str!("test.txt")), 157);
    }


    #[test]
    fn test_compute_sum_group_prio() {
        assert_eq!(compute_sum_group_prio(include_str!("test.txt")), 70);
    }
}
