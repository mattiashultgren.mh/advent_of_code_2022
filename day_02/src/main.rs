

fn compute_score(guide: &str, part: u8) -> u32 {
    guide.lines().map(|line| {
        let mut x  = line.split(' ').into_iter();

        let opponent = x.next().unwrap();
        let response = if part == 1 {
            x.next().unwrap()
        } else {
            match (opponent, x.next().unwrap()) {
                ("A", "X") | ("B", "Z") | ("C", "Y") => "Z",
                ("A", "Y") | ("B", "X") | ("C", "Z") => "X",
                ("A", "Z") | ("B", "Y") | ("C", "X") => "Y",
                x => panic!("Don't know what to do for {:?}", x)
            }
        };
        
        let points = match response {
            "X" => 1,
            "Y" => 2,
            "Z" => 3,
            _ => panic!("Unknown response!")
        } + match (opponent, response) {
            ("A", "X") | ("B", "Y") | ("C", "Z") => 3,
            ("A", "Y") | ("B", "Z") | ("C", "X") => 6,
            ("A", "Z") | ("B", "X") | ("C", "Y") => 0,
            x => panic!("Unknown playing variation! {:?}", x)
        };
        points
    }).sum::<u32>()
}


fn main() {
    println!("Part 1: {}", compute_score(include_str!("input.txt"), 1));
    println!("Part 2: {}", compute_score(include_str!("input.txt"), 2));
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_compute_score_part1() {
        assert_eq!(compute_score(include_str!("test.txt"), 1), 15);
    }


    #[test]
    fn test_compute_score_part2() {
        assert_eq!(compute_score(include_str!("test.txt"), 2), 12);
    }
}
