
fn compute_elfs_calories(list_of_numbers: &str) -> Vec<u32> {
    let data = list_of_numbers.lines()
                        .map(|line| {line.parse::<u32>()})
                        .collect::<Vec<_>>();

    let mut elfs_calories = vec![0];
    for x in data.iter() {
        if let Ok(cal) = x {
            let idx = elfs_calories.len() - 1;
            elfs_calories[idx] += cal;
        } else {
            elfs_calories.push(0);
        }
    }
    
    elfs_calories.sort_by(|a, b| b.cmp(a));

    elfs_calories
}


fn main() {
    let elfs_calories = compute_elfs_calories(include_str!("input.txt"));
    println!("Max calories on one elf is {:?}", elfs_calories.first());
    println!("The top three elfs have {} calories of food", elfs_calories.iter().take(3).sum::<u32>());
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_compute_elf_calories() {
        let elfs = compute_elfs_calories(include_str!("test.txt"));

        assert_eq!(elfs.first(), Some(&24000));
        assert_eq!(elfs.iter().take(3).sum::<u32>(), 45000);
    }
}
